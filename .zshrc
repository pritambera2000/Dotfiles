
# Load Colors
autoload -U colors && colors


# ==== Promt Like Luke If  not Using any Customize Promt e.g starshp or spaceship ====
# PS1="%B%{$fg[red]%}[%{$fg[yellow]%}%n%{$fg[green]%}@%{$fg[blue]%}%M %{$fg[magenta]%}%~%{$fg[red]%}]%{$reset_color%}$%b "

# Automatically cd into typed directory.
 setopt autocd		


# Keep 100000 lines of history within the shell and save it to ~/.zsh_history:
HISTSIZE=100000
SAVEHIST=100000
HISTFILE=~/.zsh_history

# Use modern completion system
autoload -Uz compinit
compinit

zstyle ':completion:*' auto-description 'specify: %d'
zstyle ':completion:*' completer _expand _complete _correct _approximate
zstyle ':completion:*' format 'Completing %d'
zstyle ':completion:*' group-name ''
zstyle ':completion:*' menu select=2
zstyle ':completion:*' list-prompt %SAt %p: Hit TAB for more, or the character to insert%s
zstyle ':completion:*' matcher-list '' 'm:{a-z}={A-Z}' 'm:{a-zA-Z}={A-Za-z}' 'r:|[._-]=* r:|=* l:|=*'
zstyle ':completion:*' menu select=long
zstyle ':completion:*' select-prompt %SScrolling active: current selection at %p%s
zstyle ':completion:*' use-compctl false
zstyle ':completion:*' verbose true

# Use beam shape cursor for each new prompt. &  Use beam shape cursor on startup.
echo -ne '\e[5 q' 
preexec() { echo -ne '\e[5 q' ;} 

# enable color support of ls and also add handy aliases

eval "$(dircolors -b)"
alias ls='ls --color=auto'
alias lh='ls -lah --color=auto'
alias dir='dir --color=auto'
alias vdir='vdir --color=auto'
alias grep='grep --color=auto'
alias fgrep='fgrep --color=auto'
alias egrep='egrep --color=auto'

# === Lsd command  for colored ls ==
alias ll='lsd'
alias llh='lsd -lah'

# === SETTING THE STARSHIP PROMPT ===
eval "$(starship init zsh)"
# === zsh Syntax Highlighting ===
source /home/i3wm/.zsh/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
# === Zsh Auto Suggesation ===
source ~/.zsh/zsh-autosuggestions/zsh-autosuggestions.zsh
# === RANDOM COLOR SCRIPT From DT ===
colorscript random

## ===== Prerequisites for working this config properly in a new system  ===== ##

## == Installing lsd for colorful ls output == ##
# https://github.com/Peltoche/lsd
## == Install Starship Prompt == ##
# https://starship.rs/
## == Install Zsh-Syntax-highlighting == ##
# https://github.com/zsh-users/zsh-syntax-highlighting
## == Install Zsh-autosuggsation == ##
# https://github.com/zsh-users/zsh-autosuggestions
## == Install colorsript == ##
# https://gitlab.com/dwt1/shell-color-scripts