;==========================================================
;
; THIS IS AN EXAMPLE CONFIGURATION FILE!
; IT IS NOT SUPPOSED TO WORK OUT OF THE BOX SINCE IS CONTAINS
; SETTINGS THAT ARE SPECIFIC TO THE MACHINE WHICH WAS USED
; TO GENERATE IT.
; Please refer to the web documentation hosted at:
; https://github.com/polybar/polybar#configuration
; and
; https://github.com/polybar/polybar/wiki/Configuration
; if you want to automatically generate one for you.
;
;==========================================================
;
;
;   ██████╗  ██████╗ ██╗  ██╗   ██╗██████╗  █████╗ ██████╗
;   ██╔══██╗██╔═══██╗██║  ╚██╗ ██╔╝██╔══██╗██╔══██╗██╔══██╗
;   ██████╔╝██║   ██║██║   ╚████╔╝ ██████╔╝███████║██████╔╝
;   ██╔═══╝ ██║   ██║██║    ╚██╔╝  ██╔══██╗██╔══██║██╔══██╗
;   ██║     ╚██████╔╝███████╗██║   ██████╔╝██║  ██║██║  ██║
;   ╚═╝      ╚═════╝ ╚══════╝╚═╝   ╚═════╝ ╚═╝  ╚═╝╚═╝  ╚═╝
;
;
;   To learn more about how to configure Polybar
;   go to https://github.com/polybar/polybar
;
;   The README contains a lot of information
;   
;==========================================================

[colors]
background = ${xrdb:color0:}
foreground = ${xrdb:color6:}
foreground-alt = ${xrdb:color7:}
primary = ${xrdb:color1:}
secondary = ${xrdb:color2:}
alert = ${xrdb:color3:}

[bar/example]
;monitor = ${env:MONITOR:HDMI-1}
width = 100%
height = 13
;offset-x = 1%
;offset-y = 1%
radius = 0.0
fixed-center = false

background = ${colors.background}
foreground = ${colors.foreground}

line-size = 1.99
line-color = #f00

border-size = 0
border-color = #00000000

padding-left = 0
padding-right = 1

module-margin-left = 1
module-margin-right = 0.5
module-margin-up = 0

font-0 = SpaceMono Nerd Font:style=Bold:size=9;
font-1 = HeavyData Nerd Font:style=HeavyDataNerdFontComplete:size=9.5;4
font-2 = FontAwesome5Brands:style=Regular:size=10;4

modules-left = i3
modules-center = mpd
modules-right = vpn-openvpn-isrunning backlight-acpi cmus alsa wlan eth battery date clock  powermenu

tray-position = right
tray-padding = 0
tray-background = #0063ff

;wm-restack = i3

;override-redirect = true


;scroll-up = i3wm-wsnext
;scroll-down = i3wm-wsprev

cursor-click = pointer
cursor-scroll = ns-resize

[module/xwindow]
type = internal/xwindow
label = %title:0:30:...%

[module/xkeyboard]
type = internal/xkeyboard
blacklist-0 = num lock

format-prefix = " "
format-prefix-foreground = ${colors.foreground-alt}
format-prefix-underline = ${colors.secondary}

label-layout = %layout%
label-layout-underline = ${colors.secondary}

label-indicator-padding = 2
label-indicator-margin = 1
label-indicator-background = ${colors.secondary}
label-indicator-underline = ${colors.secondary}

[module/vpn-openvpn-isrunning]
type = custom/script
exec = "~/.config/polybar/polybarscripts/vpn-openvpn-isrunning.sh"
interval = 2

[module/vpn-networkmanager-status]
type = custom/script
exec = "~/.config/polybar/polybarscripts/vpn-networkmanager-status.sh"
interval = 10

[module/speedtest]  
type = custom/script  
exec-if = hash speedtest
exec = "~/.config/polybar/polybarscripts/polybar-speedtest"  
interval = 20

[module/filesystem]
type = internal/fs
interval = 25

mount-0 = /
#mount-1 = /media/windows

label-mounted = %percentage_used%
format-mounted-prefix = "力: "
format-mounted-underline = #e647b9
format-mounted-prefix-foreground = #FFA2FF
label-unmounted = %mountpoint% not mounted
label-unmounted-foreground = #FFA2FF


[module/i3]
type = internal/i3
format = <label-state> <label-mode>
index-sort = true
wrapping-scroll = false
separator= |
; Only show workspaces on the same output as the bar
;pin-workspaces = true

label-mode-padding = 0
label-mode-foreground = #000
label-mode-background = ${colors.primary}

; focused = Active workspace on focused monitor
label-focused = %index%
label-focused-background = ${colors.background-alt}
label-focused-underline= ${colors.primary}
label-focused-padding = 1

; unfocused = Inactive workspace on any monitor
label-unfocused = %index%
label-unfocused-padding = 1

; visible = Active workspace on unfocused monitor
label-visible = %index%
label-visible-background = ${self.label-focused-background}
label-visible-underline = ${self.label-focused-underline}
label-visible-padding = ${self.label-focused-padding}

; urgent = Workspace with urgency hint set
label-urgent = %index%
label-urgent-background = ${colors.alert}
label-urgent-padding = 2

; Separator in between workspaces
;label-separator = |

[module/network-localip]
type = custom/script
exec = ~/.config/polybar/polybarscripts/network-localip.sh
interval = 10
format-prefix = " "
format-underline = #f90000

[module/info-hackspeed]
type = custom/script
exec = ~/.config/polybar/polybarscripts/info-hackspeed.sh
tail = true
interval = 0.1
format-prefix = " :"
format-prefix-foreground = #be9fe1
format-underline = #ffc58a

[module/cmus]
type = custom/script
format-prefix = " "
label = Cmus
exec = ~/.config/polybar/polybarscripts/cmus.sh
exec-if = pgrep -x cmus
interval = 1

click-middle = cmus-remote --pause
click-left = cmus-remote --next
scroll-right = cmus-remote --prev

label-maxlen = 100

[module/mpd]
type = internal/mpd
format-online = <label-song>  <icon-prev> <icon-stop> <toggle> <icon-next>

icon-prev = 
icon-stop = 
icon-play = 
icon-pause = 
icon-next = 

label-song-maxlen = 25
label-song-ellipsis = true
[module/backlight]
type = internal/backlight
card = intel_backlight
format = <label> 

[module/backlight-acpi]
inherit = module/xbacklight
type = internal/backlight
card = radeon_bl0
enable-scroll = true
;[module/backlight]
;type = internal/backlight
;card = radeon_bl0
;format = <label>
;label = %percentage%%

[module/xbacklight]
type = internal/xbacklight
format = <label> <bar>
format-prefix = "  劉 :"
format-prefix-foreground = #ffdf00
label = %percentage:%%
format-underline = #4cb0e6

bar-width = 1
bar-indicator = ""
bar-indicator-foreground = #fff
bar-indicator-font = 2
bar-fill = "盛"
bar-fill-font = 2
bar-fill-foreground = #9f78e1
bar-empty = ""
bar-empty-font = 2
bar-empty-foreground = #ffdf00
bar-fill-foreground-0 = #ffdf00
bar-fill-foreground-1 = #ffdf00 
bar-fill-foreground-2 = #ffdf00
bar-fill-foreground-3 = #ffdf00
bar-fill-foreground-4 = #ffdf00

;[module/backlightnew]
;type = custom/ipc
;hook-0 = light -S 50 | sed 's/\.[0-9]*//'
;enable-ipc = true
;format-prefix = "[backlight]"
;initial = 1


[module/cpu]
type = internal/cpu
interval = 2
format-prefix = " "
format-prefix-foreground = #3DDC84
format-underline = #f90000
label = %percentage:%

[module/memory]
type = internal/memory
interval = 2
format-prefix = ""
format-prefix-foreground = #ffa700
format-underline = #4bffdc
label = %percentage_used:3%

[module/wlan]
type = internal/network
interface = wlp4s0
interval = 3.0

format-connected = <ramp-signal> <label-connected>
format-connected-underline = #9f78e1
label-connected = %essid%%downspeed:7% %signal% ﬉
label-connected-foreground =  ${colors.foreground-alt}
;format-disconnected =
format-disconnected = <label-disconnected>
format-disconnected-underline = ${self.format-connected-underline}
label-disconnected = " 睊 "
;label-disconnected-foreground = ${colors.foreground-alt}
;%local_ip%

ramp-signal-0 = "  "
ramp-signal-1 = "  "
ramp-signal-2 = "  "
ramp-signal-3 = "  "
ramp-signal-4 = "  "
ramp-signal-foreground = #17fc03

[module/eth]
type = internal/network
interface = enp0s31f6
interval = 3.0

format-connected-underline = #55aa55
format-connected-prefix = " "
format-connected-prefix-foreground = ${colors.foreground-alt}
label-connected = 

format-disconnected =
;format-disconnected = <label-disconnected>
;format-disconnected-underline = ${self.format-connected-underline}
;label-disconnected = %ifname% disconnected
;label-disconnected-foreground = ${colors.foreground-alt}

[module/date]
type = internal/date
interval = 1

date = " %d-%m-%Y"
date-alt = " %Y-%m-%d"
format-prefix = ""
label = %date% 
format-prefix-foreground = #FFA2FF
format-underline = #0a6cf5

[module/clock]

type = internal/date
time = %H:%M:%S
time-alt = %H:%M:%S

format-prefix = " "
format-prefix-foreground = #FFA2FF
format-underline = #0a6cf5
label = %time%

[module/pulseaudio]
type = internal/pulseaudio

format-volume = <label-volume> <bar-volume>
label-volume = VOL %percentage%
label-volume-foreground = ${root.foreground}
format-volume-underline = #42d472
label-muted = 🔇 muted
label-muted-foreground = #666

bar-volume-width = 7
Sbar-volume-foreground-0 = #008744
bar-volume-foreground-1 = #55aa55
bar-volume-foreground-2 = #55aa55
bar-volume-foreground-3 = #55aa55
bar-volume-foreground-4 = #ff5e19
bar-volume-foreground-5 = #f5a70a
bar-volume-foreground-6 = #ff5555
bar-volume-underline = #ff5e19
bar-volume-gradient = false
bar-volume-indicator = |
bar-volume-indicator-font = 2
bar-volume-fill = ─
bar-volume-fill-font = 2
bar-volume-empty = ─
bar-volume-empty-font = 2
bar-volume-empty-foreground = ${colors.foreground-alt}

[module/alsa]
type = internal/alsa

format-volume = <label-volume><bar-volume>
format-volume-prefix = " :"
format-volume-prefix-foreground = #f5a70a
label-volume = %percentage%
label-volume-foreground = ${root.foreground}
format-volume-underline = #29abe3
format-muted-prefix = "ﱝ"
format-muted-foreground = ${colors.foreground-alt}
label-muted = " Muted"

bar-volume-width = 1
bar-volume-foreground-0 = #008744
bar-volume-foreground-1 = #55aa55
bar-volume-foreground-2 = #55aa55
bar-volume-foreground-3 = #55aa55
bar-volume-foreground-4 = #ff5e19
bar-volume-foreground-5 = #f5a70a
bar-volume-foreground-6 = #ff5e19
bar-volume-underline = #ff5e19
bar-volume-gradient = false
bar-volume-indicator = 
bar-volume-indicator-font = 0
bar-volume-fill = 
bar-volume-fill-font = 2
bar-volume-empty = ﭖ
bar-volume-empty-font = 2
bar-volume-empty-foreground = ${colors.foreground-alt}

[module/battery]
type = internal/battery
battery = BAT1
adapter = AC
full-at = 98

format-charging = <animation-charging> <label-charging>
format-charging-underline = #3ee319

format-discharging = <animation-discharging> <label-discharging>
format-discharging-underline = #ffb52a

format-full-prefix = ""
format-full-prefix-foreground = #ff71ce
format-full-underline = ${self.format-charging-underline}

ramp-capacity-0 = 
ramp-capacity-1 = 
ramp-capacity-2 = 
ramp-capacity-3 = 
ramp-capacity-4 = 
ramp-capacity-5 = 
ramp-capacity-6 = 
ramp-capacity-7 = 
ramp-capacity-8 = 
ramp-capacity-9 = 
ramp-capacity-10 = 
ramp-capacity-foreground = ${colors.foreground-alt}

animation-charging-0 =  
animation-charging-1 =  
animation-charging-2 =   
animation-charging-3 = 
animation-charging-4 =  
animation-charging-5 = 
#animation-charging-6 =  
animation-charging-foreground = ${colors.foreground-alt}
animation-charging-framerate = 750

animation-discharging-0 = 
animation-discharging-1 = 
animation-discharging-2 = 
animation-discharging-3 = 
animation-discharging-4 = 
animation-discharging-5 = 
animation-discharging-6 = 
animation-discharging-7 = 
animation-discharging-8 =  
animation-discharging-9 = 
animation-discharging-foreground = ${colors.foreground-alt}
animation-discharging-framerate = 750

[module/temperature]
type = internal/temperature
thermal-zone = 0
warn-temperature = 60

format = <ramp> <label>
format-underline = #f50a4d
format-warn = <ramp> <label-warn>
format-warn-underline = #faeb61

label = %temperature-c%
label-warn = %temperature-c%
label-warn-foreground = #FF0000

ramp-0 = 
ramp-1 = 
ramp-2 = 
ramp-foreground = #fc7303

[module/powermenu]
type = custom/menu

expand-right = true

format-spacing = 1

label-open = 
label-open-foreground = #FF0000
label-close =  cancel
label-close-foreground =  #faeb61
label-separator = |
label-separator-foreground = ${colors.foreground-alt}

menu-0-0 = reboot
menu-0-0-exec = reboot
menu-0-1 = power off
menu-0-1-exec = poweroff

menu-1-0 = cancel
menu-1-0-exec = menu-open-0
menu-1-1 = reboot
menu-1-1-exec = sudo reboot

menu-2-0 = power off
menu-2-0-exec =  poweroff
menu-2-1 = cancel
menu-2-1-exec = menu-open-0

[settings]
screenchange-reload = true
;compositing-background = xor
;compositing-background = screen
;compositing-foreground = source
;compositing-border = over
pseudo-transparency = true

[global/wm]
margin-top = 5
margin-bottom = 5

; vim:ft=dosini
